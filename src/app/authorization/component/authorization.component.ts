import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { AuthorizationService } from '../services/authorization.service';
import { User } from '../models/user';
import { AuthorizationHelper } from '../../helpers/user';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { AddUserModalComponent } from 'src/app/users/components/add-user-modal/add-user-modal.component';

@Component({
  selector: 'pm-authorization',
  templateUrl: './authorization.component.html',
  styleUrls: ['./authorization.component.css']
})
export class AuthorizationComponent implements OnInit {
  validationErrors = '';
  userForm = this.fb.group({
    email: [''],
    password: ['']
  });

  get isAuthorized() {
     return !this.jwtHelperService.isTokenExpired(this.jwtHelperService.tokenGetter());
  }

  get tokenData() {
    const token = this.jwtHelperService.decodeToken(this.jwtHelperService.tokenGetter());
    console.log(token);
    console.log(this.jwtHelperService.getTokenExpirationDate(this.jwtHelperService.tokenGetter()));
    return token;
  }

  get email() {
    return AuthorizationHelper.getUserData() ? AuthorizationHelper.getUserData().email : '';
  }

  constructor( private fb: FormBuilder,
               private authorizationService: AuthorizationService,
               private jwtHelperService: JwtHelperService,
               private modalService: BsModalService
               ) {
  }

  onAddUser() {
    this.openAddUserModal();
  }

  openAddUserModal() {

    const config: ModalOptions = {
      class: 'modal-dialog-centered modal-bg',
      ignoreBackdropClick: true,
      keyboard: false,
      initialState: {
      }
  };
    this.modalService.show(AddUserModalComponent,
      config);
  }

  onLogin() {
    if (this.userForm
      && this.userForm.get('email').value
      && this.userForm.get('password').value) {
        const user: User = {
          email: this.userForm.get('email').value,
          password: this.userForm.get('password').value
        };

        this.authorizationService.login(user);
      }
  }

  ngOnInit() {
  }

}
