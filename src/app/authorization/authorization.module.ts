import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AuthorizationComponent } from './component/authorization.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AddUserModalComponent } from '../users/components/add-user-modal/add-user-modal.component';


const routes: Routes = [
  { path: '', component: AuthorizationComponent }
];



@NgModule({
  declarations: [
    AuthorizationComponent,
    AddUserModalComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ModalModule.forRoot(),
    ReactiveFormsModule,
  ],
  entryComponents: [
    AddUserModalComponent
  ]
})
export class AuthorizationModule { }
