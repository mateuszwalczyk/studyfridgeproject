import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { ReadKeyExpr } from '@angular/compiler';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthorizationService {

  constructor(private jwtHelperService: JwtHelperService,
              private httpClient: HttpClient) {
  }

  login(user: User) {
    this.httpClient.post<string>('http://localhost/StudyFridge.Auth.Api/auth/authenticate', {
      email: user.email,
      password: user.password
    }).subscribe(response => {
      sessionStorage.setItem('access_token', response);
    });
  }
}
