import { Injectable } from '@angular/core';
import { IRecipe } from '../models/recipe-model';
import { IRecipeDetails } from '../models/recipe-details-model';
import { Recipe } from '../models/recipe';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IRateRecipe } from '../models/rate-recipe';

@Injectable({
  providedIn: 'root'
})
export class RecipeService {
  constructor(private httpClient: HttpClient) {
    this.getRecipes();
  }

  public addRecipe(recipeDetails: IRecipeDetails) {
    this.httpClient.put<string>('http://localhost/StudyFridge.Recipe.Api/recipe/AddRecipe', {
      name: recipeDetails.name,
      description: recipeDetails.description,
      starRating: recipeDetails.starRating,
      Ingredients: recipeDetails.ingredients
    }).subscribe();
  }

  public rateRecipe(rateRecipe: IRateRecipe) {
    this.httpClient.put<string>('http://localhost/StudyFridge.Recipe.Api/recipe/RateRecipe', {
      recipeId: rateRecipe.recipeId,
      rateRecipeValue: rateRecipe.rateRecipeValue
    }).subscribe();
  }

  public getRecipes(recipeName?: string): Observable<[IRecipe]> {
    return this.httpClient.get<[IRecipe]>('http://localhost/StudyFridge.Recipe.Api/recipe/getrecipes', {
      params: { recipeName: recipeName ? recipeName : '' }
    });
  }

  public getRecipeDetails(recipeId: number): Observable<IRecipeDetails> {
    return this.httpClient.get<IRecipeDetails>('http://localhost/StudyFridge.Recipe.Api/recipe/getrecipedetails', {
      params: { recipeId: recipeId.toString() }
    });
  }

  public searchRecipe(ingredientName?: string): Observable<[IRecipe]> {
    return this.httpClient.get<[IRecipe]>('http://localhost/StudyFridge.Recipe.Api/recipe/searchrecipe', {
      params: {ingredientName: ingredientName ? ingredientName : '' }
    });
  }

}
