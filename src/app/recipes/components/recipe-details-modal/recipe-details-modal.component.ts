import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { IRecipeDetails } from '../../models/recipe-details-model';
import { IRecipe } from '../../models/recipe-model';

@Component({
  selector: 'pm-recipe-details-modal',
  templateUrl: './recipe-details-modal.component.html',
  styleUrls: ['./recipe-details-modal.component.css']
})
export class RecipeDetailsModalComponent implements OnInit {
  imageWidth = 400;
  imageHeight = 150;
  imageMargin = 30;

  model: IRecipeDetails;

  constructor(public modalRef: BsModalRef) {
  }
  ngOnInit() {
  }
}
