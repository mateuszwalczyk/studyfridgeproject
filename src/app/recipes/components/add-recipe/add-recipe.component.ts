import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { RecipeService } from '../../services/recipe.service';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { IRecipeDetails } from '../../models/recipe-details-model';
import { IIngredient } from '../../models/ingredient-model';

@Component({
  selector: 'pm-add-recipe',
  templateUrl: './add-recipe.component.html',
  styleUrls: ['./add-recipe.component.css']
})
export class AddRecipeComponent implements OnInit {
  ingredientsFormArray: FormGroup[] = [];
  recipeForm = this.fb.group({
    name: [''],
    description: [''],
  });

  constructor(public modalRef: BsModalRef,
              private fb: FormBuilder,
              private recipeService: RecipeService) {
  }

  onAddIngredients() {
    this.ingredientsFormArray.push(this.fb.group({
      name: [''],
      quantity: ['']
    }));
  }

  onSubmit() {
    const result: IRecipeDetails = {
      name: this.recipeForm.get('name').value,
      description: this.recipeForm.get('description').value,
      ingredients: this.getIngredients()
    };

    this.recipeService.addRecipe(result);
    this.modalRef.hide();
  }

  private getIngredients(): IIngredient[] {
    const ingredients = [];
    this.ingredientsFormArray.forEach(element => {
      ingredients.push({
        name: element.get('name').value,
        quantity: element.get('quantity').value
      });
    });

    return ingredients;
  }

  ngOnInit() {
  }

}
