import { Component, OnInit, Input } from '@angular/core';
import { RecipeService } from '../../services/recipe.service';
import { IRecipe } from '../../models/recipe-model';
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { RecipeDetailsModalComponent } from '../../components/recipe-details-modal/recipe-details-modal.component';
import { IRecipeDetails } from '../../models/recipe-details-model';
import { AddRecipeComponent } from '../add-recipe/add-recipe.component';

@Component({
    selector: 'pm-recipes',
    templateUrl: './recipe-list.component.html',
    styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

    pageTitle = 'Lista przepisów';
    imageWidth = 50;
    imageMargin = 2;
    showIngredients = false;

    _listFilter = '';
    get listFilter(): string {
        return this._listFilter;
    }
    set listFilter(value: string) {
        this._listFilter = value;
        this.recipeService.getRecipes(this.listFilter).subscribe(result => this.recipes = result);
    }

    _searchRecipe = '';
    get searchRecipe(): string {
        return this._searchRecipe;
    }
    set searchRecipe(value: string) {
        this._searchRecipe = value;
        this.recipeService.searchRecipe(this.searchRecipe).subscribe(result => this.recipes = result);
    }
    recipes: IRecipe[];

    constructor(private recipeService: RecipeService,
                private modalService: BsModalService) {

    }

    openRecipeDetailsModal(recipeId: number) {
        this.recipeService.getRecipeDetails(recipeId).subscribe(result => {
            const config: ModalOptions = {
                class: 'modal-dialog-centered modal-bg',
                ignoreBackdropClick: true,
                keyboard: false,
                initialState: {
                    model: result
                }
            };
            this.modalService.show(RecipeDetailsModalComponent,
                config);
        });
    }

    onAddRecipe() {
        this.openAddRecipeModal();
      }

    openAddRecipeModal() {
        const config: ModalOptions = {
            class: 'modal-dialog-centered modal-bg',
            ignoreBackdropClick: true,
            keyboard: false,
            initialState: {
            }
        };
        this.modalService.show(AddRecipeComponent,
            config);
    }

    ngOnInit(): void {
        this.recipeService.getRecipes(this.listFilter).subscribe(result => this.recipes = result);
    }

}


