import { Component, Input } from '@angular/core';
import { IRateRecipe } from '../../models/rate-recipe';
import { FormGroup, FormBuilder, NgModel } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { RecipeService } from '../../services/recipe.service';
import { RecipeDetailsModalComponent } from '../recipe-details-modal/recipe-details-modal.component';

@Component({
  selector: 'pm-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.css'],
})
export class RatingComponent {
  max = 5;
  rateRecipeValue: number;
  @Input() rateRecipeId: number;

  constructor(public modalRef: BsModalRef,
              private recipeService: RecipeService) { }

  onAddRating() {
    this.recipeService.rateRecipe({recipeId: this.rateRecipeId, rateRecipeValue: this.rateRecipeValue});
  }
}
