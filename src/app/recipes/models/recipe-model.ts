export interface IRecipe {
    recipeId?: number;
    name?: string;
    starRating?: number;
}
