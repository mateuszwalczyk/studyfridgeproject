export class Recipe {
    recipeId: number;
    name?: string;
    starRating?: number;
    imageUrl?: string;
}
