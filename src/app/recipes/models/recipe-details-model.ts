import { IRecipe } from './recipe-model';
import { IIngredient } from './ingredient-model';

export interface IRecipeDetails extends IRecipe {
    recipeDetailId?: number;
    ingredients: IIngredient[];
    description: string;
}
