import { IRecipe } from './recipe-model';

export interface IRateRecipe extends IRecipe {
    rateRecipeValue: number;
}
