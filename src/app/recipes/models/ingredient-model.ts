export interface IIngredient {
    ingredientId?: number;
    name: string;
    quantity: string;
}
