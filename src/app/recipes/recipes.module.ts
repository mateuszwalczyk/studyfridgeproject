import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { RecipeListComponent } from './components/recipe-list/recipe-list.component';
import { StarComponent } from './components/star/star.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RecipeDetailsModalComponent } from './components/recipe-details-modal/recipe-details-modal.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { AddRecipeComponent } from './components/add-recipe/add-recipe.component';
import { RatingComponent } from './components/rating/rating.component';
import { RatingModule } from 'ngx-bootstrap/rating';


const routes: Routes = [
  { path: '', component: RecipeListComponent }
];

@NgModule({
  declarations: [
    RecipeListComponent,
    StarComponent,
    RecipeDetailsModalComponent,
    AddRecipeComponent,
    RatingComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    ModalModule.forRoot(),
    RatingModule.forRoot()
  ],
  entryComponents: [
    RecipeDetailsModalComponent,
    AddRecipeComponent,
    StarComponent,
  ],
})
export class RecipesModule { }
