import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { IUser } from '../../models/user-modal';
import { FormControl, FormBuilder, Validators, ValidatorFn, ValidationErrors, FormGroup } from '@angular/forms';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'pm-add-user-modal',
  templateUrl: './add-user-modal.component.html',
  styleUrls: ['./add-user-modal.component.css']
})
export class AddUserModalComponent implements OnInit {
  validationErrors = '';
  userForm = this.fb.group({
    email: [''],
    password: ['']
  },
    {
      validators: this.validator.bind(this)
    });

  constructor(public modalRef: BsModalRef,
              private fb: FormBuilder,
              private userService: UserService) {
  }

  onSubmit() {
    const userEmail = this.userForm.get('email').value;
    const userPassword = this.userForm.get('password').value;

    if (this.userForm.valid) {
      this.userService.addUser({ email: userEmail, password: userPassword });
      this.modalRef.hide();
    } else {
      this.validationErrors = this.userForm.errors.email;
    }
  }

  private validator(control: FormGroup): ValidationErrors | null {
    const email = control.get('email');
    const password = control.get('password');
    const hasValue = email && email.value && password && password.value;

    if (!hasValue) {
      return {
        email: 'Wartość wymagana',
        password: 'Wartość wymagana'
      };
    }

    if (this.userService.userExist({
      email: email.value,
      password: password.value
    })) {
      return { email: 'Istnieje użytkownik o podanym adresie email' };
    }

    return null;
  }

  ngOnInit() {
  }

}
