import { Component, OnInit } from '@angular/core';
import { User } from '../../../authorization/models/user';
import { UserService } from '../../services/user.service';
import { BsModalService, ModalOptions } from 'ngx-bootstrap/modal';
import { IUser } from '../../models/user-modal';

@Component({
  selector: 'pm-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  users: IUser[];
  pageTitle = 'Lista użytkowników';

  constructor(private userService: UserService) {

  }

  ngOnInit() {
    this.userService.getUsers().subscribe(response => this.users = response);
  }

}
