import { Injectable } from '@angular/core';
import { IUser } from '../models/user-modal';
import { User } from 'src/app/authorization/models/user';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) {
    // this.getUsers();
  }

  users: IUser[];

  addUser(user: User) {
    this.httpClient.post<string>('http://localhost/StudyFridge.Auth.Api/auth/adduser', {
      email: user.email,
      password: user.password
    }).subscribe();
  }

  public userExist(user: IUser) {
    return this.users.some(x => x.email === user.email);
  }

  public getUsers(): Observable<[IUser]> {
    return this.httpClient.get<[IUser]>('http://localhost/StudyFridge.Auth.Api/auth/getusers');
  }

}
