import { UserData } from '../common/user-data';

export class AuthorizationHelper {
    public static isAuthorized(): boolean {
        return sessionStorage.getItem('userData') ? true : false;
    }

    public static getUserData(): UserData {
        if (this.isAuthorized()) {
            return JSON.parse(sessionStorage.getItem('userData'));
        }
    }
}
