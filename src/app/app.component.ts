import { Component } from '@angular/core';
import { AuthorizationHelper } from './helpers/user';

@Component({
  selector: 'pm-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  pageTitle = 'StudyFridge';

  get isAuthorized() {
    return AuthorizationHelper.isAuthorized();
  }
}
